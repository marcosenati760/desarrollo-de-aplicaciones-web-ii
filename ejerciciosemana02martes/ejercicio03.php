
<?php
// Variables
$inicio = 0;
$fin = 0;
$cantidad_capicuas = 0;

if (isset($_POST["btnCalcular"])) {
    // Obtener valores ingresados por el usuario
    $inicio = (int)$_POST["txtInicio"];
    $fin = (int)$_POST["txtFin"];

    // Validar entrada
    if ($inicio < $fin) {
        // Contar números capicúa
        for ($i = $inicio; $i <= $fin; $i++) {
            if (es_capicua($i)) {
                $cantidad_capicuas++;
            }
        }
    }
}

function es_capicua($numero) {
    $numero_invertido = strrev($numero);
    return $numero == $numero_invertido;
}
?>

<html>
<head>
    <title>Cantidad de números capicúa en un rango</title>
    <style type="text/css" >
        .TextoFondo {
            background-color: #CCFFFF;
        }
    </style>
    
    <link rel="stylesheet" href="ejercicio03.css">


</head>

<body>
    <form method="post" action="">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Cantidad de números capicúa en un rango</strong></td>
            </tr>
            <tr>
                <td>Inicio:</td>
                <td>
                    <input name="txtInicio" type="text" id="txtInicio" value="<?= $inicio ?>" />
                </td>
            </tr>
            <tr>
                <td>Fin:</td>
                <td>
                    <input name="txtFin" type="text" id="txtFin" value="<?= $fin ?>" />
                </td>
            </tr>
            <tr>
                <td>Cantidad de capicuas:</td>
                <td>
                    <input name="txtCapicuas" type="text" class="TextoFondo" id="txtCapicuas" value="<?= $cantidad_capicuas ?>" readonly />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>




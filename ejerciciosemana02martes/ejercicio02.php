<?php

if (isset($_POST['btnCalcular'])) {
    
    $a = (int)$_POST['txtA'];
    $b = (int)$_POST['txtB'];
    
    
    $suma_pares = 0;
    $cant_pares = 0;
    $suma_impares = 0;
    $cant_impares = 0;
    $suma_multiplos_3 = 0;
    $cant_multiplos_3 = 0;
    
    
    for ($i = $a; $i <= $b; $i++) {
        if ($i % 2 == 0) { 
            $suma_pares += $i; 
            $cant_pares++; 
        } else { 
            $suma_impares += $i; 
            $cant_impares++; 
        }
        
        if ($i % 3 == 0) { 
            $suma_multiplos_3 += $i; 
            $cant_multiplos_3++;
        }
    }
}
?>

<html>
<head>
    <title>Suma y cantidad de números en un rango</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
    </style>
</head>

<body>
<link rel="stylesheet" type="text/css" href="ejercicio02.css">

    <form method="post" action="ejercicio02.php">
        <table width="300" border="0">
            <tr>
                <td colspan="2"><strong>Suma y cantidad de números/rango</strong></td>
            </tr>
            <tr>
                <td>Ingrese valor de a:</td>
                <td>
                    <input name="txtA" type="text" id="txtA" value="<?= isset($a) ? $a : '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese  valor de b:</td>
                <td>
                    <input name="txtB" type="text" id="txtB" value="<?= isset($b) ? $b : '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Suma de los números pares:</td>
                <td>
                    <input name="txtSumaPares" type="text" class="TextoFondo" id="txtSumaPares" value="<?= isset($suma_pares) ? $suma_pares : '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Cantidad de los números pares:</td>
                <td>

   
                    <input name="txtCantPares" type="text" class="TextoFondo" id="txtcantidadpares" value="<?= isset($cant_impares) ?$cant_impares  : '' ?>"/>
            </td>
              <tr>
    <td></td>
    <td>
        <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
    </td>
    </tr>
</table>
</form>
</body>

</html>
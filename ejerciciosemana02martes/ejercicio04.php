<?php
// Variables
$n = 0;
$contador = 0;

// Función para determinar si un número es primo
function esPrimo($numero) {
    if($numero < 2) {
        return false;
    }
    for($i = 2; $i <= $numero/2; $i++) {
        if($numero % $i == 0) {
            return false;
        }
    }
    return true;
}

if(isset($_POST["btnCalcular"])) {
    $n = (int)$_POST["txtn1"];
    $inicio = pow(10, $n - 1);
    $fin = pow(10, $n) - 1;
    for($i = $inicio; $i <= $fin; $i++) {
        if(esPrimo($i)) {
            $contador++;
        }
    }
}
?>

<html>
<head>
    <title>Cantidad de números primos de n cifras</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
    </style>
</head>

<body>
    <link rel="stylesheet" href="ejercicio04.css">
    <form method="post" action="ejercicio04.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Cantidad de números primos de n cifras</strong></td>
            </tr>
            <tr>
                <td>Ingrese N:</td>
                <td>
                    <input name="txtn1" type="text" id="txtn1" value="<?= $n ?>" />
                </td>
            </tr>
            <tr>
                <td>Cantidad de números primos:</td>
                <td>
                    <input name="txts" type="text" class="TextoFondo" id="txts" value="<?= $contador ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

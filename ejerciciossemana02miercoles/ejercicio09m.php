<!DOCTYPE html>
<html>
<head>
	<title>Contador de Palíndromos</title>
	
</head>
<body>
    <link rel="stylesheet" href="estilo09.css">
	<h1>Contador de Palíndromos</h1>
	<form method="post">
		<label for="frase">Ingrese una frase:</label>
		<input type="text" id="frase" name="frase" required>
		<input type="submit" value="Contar Palíndromos">
	</form>
	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$frase = $_POST["frase"];
			$palabras = explode(" ", $frase);
			$palindromos = 0;
			foreach ($palabras as $palabra) {
				if (strtolower($palabra) == strtolower(strrev($palabra))) {
					$palindromos++;
				}
			}
			echo "<p>La frase \"$frase\" tiene $palindromos palabra(s) palíndroma(s).</p>";
		}
	?>
</body>
</html>

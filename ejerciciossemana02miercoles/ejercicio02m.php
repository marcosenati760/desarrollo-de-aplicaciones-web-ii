<?php
// Verificar si se ha enviado el formulario
if(isset($_POST['btnCalcular'])) {
    // Recoger los valores de los input
    $numeros = array(
        $_POST['num1'],
        $_POST['num2'],
        $_POST['num3'],
        $_POST['num4'],
        $_POST['num5'],
        $_POST['num6']
    );
    $n = $_POST['n'];
    
    // Inicializar el contador de múltiplos
    $multiplos = 0;
    
    // Iterar sobre los números y contar los múltiplos
    foreach($numeros as $numero) {
        if($numero % $n == 0) {
            $multiplos++;
        }
    }
}
?>

<html>
<head>
    <title>Múltiplos de N</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
    </style>
</head>

<body>
    <link rel="stylesheet" href="estilo02.css">
    <form method="post" action="">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Múltiplos de N</strong> </td>
            </tr>
            <tr>
                <td>Ingrese N: </td>
                <td>
                    <input name="n" type="text" id="n" value="<?= isset($n) ? $n : '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese los números: </td>
                <td>
                    <input name="num1" type="text" id="num1" value="<?= isset($_POST['num1']) ? $_POST['num1'] : '' ?>" />
                    <input name="num2" type="text" id="num2" value="<?= isset($_POST['num2']) ? $_POST['num2'] : '' ?>" />
                    <input name="num3" type="text" id="num3" value="<?= isset($_POST['num3']) ? $_POST['num3'] : '' ?>" />
                    <input name="num4" type="text" id="num4" value="<?= isset($_POST['num4']) ? $_POST['num4'] : '' ?>" />
                    <input name="num5" type="text" id="num5" value="<?= isset($_POST['num5']) ? $_POST['num5'] : '' ?>" />
                    <input name="num6" type="text" id="num6" value="<?= isset($_POST['num6']) ? $_POST['num6'] : '' ?>" />
                </td>
            </tr>
            <tr>
                <td>Múltiplos de N:</td>
                <td>
                    <input name="txtMultiplos" type="text" class="TextoFondo" id="txtMultiplos" value="<?= isset($multiplos) ? $multiplos : '' ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

<?php
$A = array(
    array(1, 2),
    array(3, 4)
);

$B = array(
    array(5, 6),
    array(7, 8)
);

$C = array(
    array(0, 0),
    array(0, 0)
);


for ($i = 0; $i < 2; $i++) {
    for ($j = 0; $j < 2; $j++) {
        for ($k = 0; $k < 2; $k++) {
            $C[$i][$j] += $A[$i][$k] * $B[$k][$j];
        }
    }
}


echo "Matriz A:<br>";
for ($i = 0; $i < 2; $i++) {
    for ($j = 0; $j < 2; $j++) {
        echo $A[$i][$j] . " ";
    }
    echo "<br>";
}

echo "<br>Matriz B:<br>";
for ($i = 0; $i < 2; $i++) {
    for ($j = 0; $j < 2; $j++) {
        echo $B[$i][$j] . " ";
    }
    echo "<br>";
}

echo "<br>Matriz C (resultado de A * B):<br>";
for ($i = 0; $i < 2; $i++) {
    for ($j = 0; $j < 2; $j++) {
        echo $C[$i][$j] . " ";
    }
    echo "<br>";
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="estilo.css">
    <title>Multiplicación de matrices</title>
    <style type="text/css">
        body {
            background-color: #F5F5F5;
            font-family: Arial, sans-serif;
        }
    <div id="contenedor">
        <h1>Multiplicación de matrices</h1>
        <table class="matrizA">
            <tr>
                <th>A[0][0]</th>
                <th>A[0][1]</th>
            </tr>
            <tr>
                <td><input name="txta11" type="text" value="1"></td>
                <td><input name="txta12" type="text" value="2"></td>
            </tr>
            <tr>
                <th>A[1][0]</th>
                <th>A[1][1]</th>
            </tr>
            <tr>
                <td><input name="txta21" type="text" value="3"></td>
                <td><input name="txta22" type="text" value="4"></td>
            </tr>
        </table>
        <br>
        <table class="matrizB">
            <tr>
                <th>B[0][0]</th>
                <th>B[0][1]</th>
            </tr>
            <tr>
                <td><input name="txtb11" type="text" value="5"></td>
                <td><input name="txtb12" type="text" value="6"></td>
            </tr>
            <tr>
                <th>B[1][0]</th>
                <th>B[1][1]</th>
            </tr>
            <tr>
                <td><input name="txtb21" type="text" value="7"></td
            </tr>
        </table>
    </body>
</head>
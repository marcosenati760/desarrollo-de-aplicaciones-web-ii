
<!DOCTYPE html>
<html>
<head>
	<title>Búsqueda de Frase</title>
	
</head>
<body>
    <link rel="stylesheet" href="estilo08.css">
	<h1>Búsqueda de Frase</h1>
	<?php
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$frase = $_POST['frase'];
			$clave = $_POST['clave'];
			if (!empty($frase) && !empty($clave)) {
				if (strpos($frase, $clave) !== false) {
					echo '<p class="success">La palabra "' . $clave . '" se encuentra en la frase.</p>';
				} else {
					echo '<p class="error">La palabra "' . $clave . '" no se encuentra en la frase.</p>';
				}
			} else {
				echo '<p class="error">Por favor ingrese la frase y la palabra clave.</p>';
			}
		}
	?>
	<form method="post">
		<label for="frase">Ingrese la frase:</label>
		<textarea id="frase" name="frase" rows="5" cols="40"><?php if(isset($_POST['frase'])) echo $_POST['frase']; ?></textarea>
		<label for="clave">Ingrese la palabra clave:</label>
		<input type="text" id="clave" name="clave" value="<?php if(isset($_POST['clave'])) echo $_POST['clave']; ?>">
		<input type="submit" value="Buscar">
	</form>
</body>
</html>

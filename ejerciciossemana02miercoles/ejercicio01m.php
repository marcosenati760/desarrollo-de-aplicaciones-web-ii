<!-- Dados 4 números y almacénelo en un vector, luego obtenga la suma y el promedio de los
valores almacenados. -->


<?php
if (isset($_POST["btnCalcular"])) {
    // Obtener los valores del formulario y guardarlos en un array
    $numeros = [
        (int)$_POST["numero1"],
        (int)$_POST["numero2"],
        (int)$_POST["numero3"],
        (int)$_POST["numero4"]
    ];
    
    // Calcular la suma de los valores
    $suma = array_sum($numeros);
    
    // Calcular el promedio de los valores
    $promedio = $suma / count($numeros);
}
?>

<html>
<head>
    <title>Suma y promedio de números</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
    </style>
</head>

<body>

    <link rel="stylesheet" href="estilo01.css">
    <form method="post" action="">
        <table>
        <tr>
    <td colspan="2"><strong>SUMA Y PROMEDIO DE NUMEROS</strong> </td>
</tr>
            <tr>
                <td>Ingrese número 1: </td>
                <td><input type="number" name="numero1"></td>
            </tr>
            <tr>
                <td>Ingrese número 2: </td>
                <td><input type="number" name="numero2"></td>
            </tr>
            <tr>
                <td>Ingrese número 3: </td>
                <td><input type="number" name="numero3"></td>
            </tr>
            <tr>
                <td>Ingrese número 4: </td>
                <td><input type="number" name="numero4"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="btnCalcular" value="Calcular">
                </td>
            </tr>
            <?php if (isset($_POST["btnCalcular"])): ?>
                <tr>
                    <td>Suma:</td>
                    <td>
                        <input type="text" class="TextoFondo" value="<?= $suma ?>" readonly>
                    </td>
                </tr>
                <tr>
                    <td>Promedio:</td>
                    <td>
                        <input type="text" class="TextoFondo" value="<?= $promedio ?>" readonly>
                    </td>
                </tr>
            <?php endif; ?>
        </table>
    </form>
</body>
</html>

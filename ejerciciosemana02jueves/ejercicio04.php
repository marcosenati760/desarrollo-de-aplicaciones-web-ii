<?php
function Recargo($tipo_cliente){
    if($tipo_cliente == 'G'){
        return 0.1;
    } elseif($tipo_cliente == 'A'){
        return 0.05;
    }
    return 0;
}

function Descuento($tipo_cliente){
    if($tipo_cliente == 'G'){
        return 0.20;
    } elseif($tipo_cliente == 'A'){
        return 0.2;
    }
    return 0;
}

if(isset($_POST["btnCalcular"])) {
    $tipo_cliente = $_POST["cmbTipoCliente"];
    $forma_pago = $_POST["cmbFormaPago"];
    $monto_compra = (float)$_POST["txtMontoCompra"];
    $descuento = Descuento($tipo_cliente);
    $recargo = Recargo($tipo_cliente);

    if($forma_pago == 'P'){
        $recargo *= -1;
    }

    $monto_descuento = $monto_compra * $descuento;
    $monto_recargo = $monto_compra * $recargo;
    $monto_total = $monto_compra + $monto_recargo - $monto_descuento;
}
?>
<html>
<head>
    <title>Descuento y recargo en compra</title>
</head>
<body>
    <link rel="stylesheet" href="estilo04.css">
    <h1>Descuento y recargo en compra</h1
    >
    <form method="post" action="">
        <table>
            <tr>
                <td><label for="cmbTipoCliente">Tipo de cliente:</label></td>
                <td>
                    <select id="cmbTipoCliente" name="cmbTipoCliente">
                        <option value="G">Público en general</option>
                        <option value="A">Cliente afiliado</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="cmbFormaPago">Forma de pago:</label></td>
                <td>
                    <select id="cmbFormaPago" name="cmbFormaPago">
                        <option value="C">Contado</option>
                        <option value="P">Plazos</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="txtMontoCompra">Monto de la compra:</label></td>
                <td><input type="number" id="txtMontoCompra" name="txtMontoCompra" value="<?= isset($_POST["txtMontoCompra"]) ? $_POST["txtMontoCompra"] : "" ?>" /></td>
            </tr>
            <tr>
                <td><label for="txtMontoDescuento">Monto del descuento:</label></td>
                <td><input type="number" id="txtMontoDescuento" name="txtMontoDescuento" value="<?= isset($monto_descuento) ? $monto_descuento : "" ?>" readonly /></td>
            </tr>
            <tr>
                <td><label for="txtMontoRecargo">Monto del recargo:</label></td>
                <td><input type="number" id="txtMontoRecargo" name="txtMontoRecargo" value="<?= isset($monto_recargo) ? $monto_recargo : "" ?>" readonly /></td>
            </tr>
            <tr>
                <td><label for="txtMontoTotal">Total a pagar:</label></td>
                <td><input type="number" id="txtMontoTotal" name="txtMontoTotal" value="<?= isset($monto_total) ? $monto_total : "" ?>" readonly /></td>
            </tr>
        </table>
        <input type="submit" name="btnCalcular" value="Calcular" />
    </form>
</body>
</html>
<?php

function Cuadrado($lado, &$area, &$perimetro) {
    $area = pow($lado, 2);
    $perimetro = $lado * 4;
}

if(isset($_POST["btnCalcular"])) {
    $lado = (float)$_POST["txtLado"];
    $area = 0;
    $perimetro = 0;
    Cuadrado($lado, $area, $perimetro);
}
?>

<html>
<head>
    <title>Área y Perímetro de un Cuadrado</title>
</head>
<body>
    <link rel="stylesheet" href="estilo03.css">
    
    <h1>Área y Perímetro de un Cuadrado</h1>
    <form method="post" action="">
        <table>
            <tr>
                <td><label for="txtLado">Lado:</label></td>
                <td><input type="number" id="txtLado" name="txtLado" value="<?= isset($_POST["txtLado"]) ? $_POST["txtLado"] : "" ?>" /></td>
            </tr>
            <tr>
                <td><label for="txtArea">Área:</label></td>
                <td><input type="number" id="txtArea" name="txtArea" value="<?= isset($area) ? $area : "" ?>" readonly /></td>
            </tr>
            <tr>
                <td><label for="txtPerimetro">Perímetro:</label></td>
                <td><input type="number" id="txtPerimetro" name="txtPerimetro" value="<?= isset($perimetro) ? $perimetro : "" ?>" readonly /></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="btnCalcular" value="Calcular" /></td>
            </tr>
        </table>
    </form>
    
    
    
</body>
</html>

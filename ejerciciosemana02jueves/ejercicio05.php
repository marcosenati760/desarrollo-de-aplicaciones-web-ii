<?php
function esPalindromo($palabra) {
    $largo = strlen($palabra);
    for($i = 0; $i < $largo / 2; $i++) {
        if($palabra[$i] !== $palabra[$largo - $i - 1]) {
            return false;
        }
    }
    return true;
}


?>
<!DOCTYPE html>
<html>
<head>
	<title>Palíndromo</title>
</head>
<body>
    <link rel="stylesheet" href="estilo05.css">
	<h1>Palíndromo</h1>
	<form method="post" action="">
		<label for="palabra">Ingrese una palabra:</label>
		<input type="text" id="palabra" name="palabra">
		<input type="submit" name="btnVerificar" value="Verificar">
	</form>
	<?php
		if(isset($_POST["btnVerificar"])) {
			$palabra = $_POST["palabra"];
			$es_palindromo = true;
			$longitud = strlen($palabra);
			for($i=0; $i<$longitud/2; $i++) {
				if($palabra[$i] != $palabra[$longitud-$i-1]) {
					$es_palindromo = false;
					break;
				}
			}
			
		}
	?>
    <div class="salida">
        <?php 
        if($es_palindromo) {
				echo "<p>La palabra \"$palabra\" es un palíndromo.</p>";
			} else {
				echo "<p>La palabra \"$palabra\" no es un palíndromo.</p>";
			}
            ?></div>
</body>
</html>

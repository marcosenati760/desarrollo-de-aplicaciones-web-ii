<?php
function determinarCaracter($caracter) {
    if (ctype_alpha($caracter)) {
        if (ctype_upper($caracter)) {
            echo "$caracter es una letra mayúscula.";
        } else {
            echo "$caracter es una letra minúscula.";
        }
    } elseif (ctype_digit($caracter)) {
        echo "$caracter es un número.";
    } else {
        echo "$caracter es un símbolo.";
    }

    $vocales = array("a", "e", "i", "o", "u");
    if (in_array(strtolower($caracter), $vocales)) {
        echo " Además, es una vocal.";
    }
}

if(isset($_POST["btnVerificar"]))
?>

<html>
<head>
    <title>Determinar Caracter</title>
</head>
<body>
    <link rel="stylesheet" href="estilo03.css">
    
    <h1>Determinar que Caracter es:</h1>
    <form method="post" action="ejericicio03.php">
        <table>
            <tr>
                <td><label for="txtCaracter">Caracter:</label></td>
                <td><input type="text" id="txtCaracter" name="txtCaracter" value="<?= isset($_POST["txtCaracter"]) ? $_POST["txtCaracter"] : "" ?>" /></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="btnVerificar" value="Verificar" /></td>
            </tr>
        </table>
        <tr class="comparacion"> <?PHP {
    $caracter = $_POST["txtCaracter"];determinarCaracter($caracter);
}
?>
</tr>
    </form>
    
</body>
</html>


<html>
<head>
	<title>Etapa de vida</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
</head>
<body>
    <link rel="stylesheet" href="estilo02.css">
	<h1>Etapa de vida</h1>
	<form method="post" action="">
		<label for="edad">Ingrese su edad:</label>
		<input type="number" id="edad" name="edad" min="0" max="150" required>

		<input type="submit" value="Calcular">

		<?php
			if(isset($_POST["edad"])) {
				$edad = (int)$_POST["edad"];
				$etapa = "";

				if($edad >= 0 && $edad <= 2) {
					$etapa = "Bebé";
				} elseif($edad >= 3 && $edad <= 5) {
					$etapa = "Niño";
				} elseif($edad >= 6 && $edad <= 12) {
					$etapa = "Pubertad";
				} elseif($edad >= 13 && $edad <= 18) {
					$etapa = "Adolescente";
				} elseif($edad >= 19 && $edad <= 25) {
					$etapa = "Joven";
				} elseif($edad >= 26 && $edad <= 60) {
					$etapa = "Adulto";
				} elseif($edad > 60) {
					$etapa = "Anciano";
				}

				if(!empty($etapa)) {
					echo "<p>Usted está en la etapa de: <span class='resultado'>$etapa</span></p>";
				} else {
					echo "<p class='resultado'>Edad no válida</p>";
				}
			}
		?>
	</form>
</body>
